import React, { Component } from 'react';
import Choropleth from 'react-leaflet-choropleth'
import { Map } from 'react-leaflet'

class App extends Component {

  fetchJSON = (url) => {
    return fetch(url)
      .then(function (response) {
        return response.json();
      });
  }

  render() {

    var data = this.fetchJSON('./assets/census_tract.geojson')
      .then(function (data) { return data })

    const style = {
      fillColor: '#F28F3B',
      weight: 2,
      opacity: 1,
      color: 'white',
      dashArray: '3',
      fillOpacity: 0.5
    }

    return (
      <div>
        <Map>
          <Choropleth
            data={{ type: 'FeatureCollection', features: data }}
            valueProperty={(feature) => feature.properties.value}
            visible={(feature) => feature.id !== active.id}
            scale={['#b3cde0', '#011f4b']}
            steps={7}
            mode='e'
            style={style}
            onEachFeature={(feature, layer) => layer.bindPopup(feature.properties.label)}
            ref={(el) => this.choropleth = el.leafletElement}
          />
        </Map>
      </div>
    );
  }
}

export default App;